# Nat Type Tester

use pystun in docker to test host machine's nat type

## Usage

``` bash
docker run --rm --network=host registry.gitlab.com/tiehichi/nat-type-tester:latest
```

or you can use other arguments, see help like this

``` bash
docker run --rm --network=host registry.gitlab.com/tiehichi/nat-type-tester:latest pystun -h
```